# GUI Quantum Wire Simulator
This is a sumulator based on the qantum wire saddle point model. The project is based on the main program in:

https://github.com/EricCWWong/ConductanceSimulator

Unlike the main package, this GUI version do not require pre-installation.

# Usage
To use the program, simply run the python file `gui.py` in command line.

`$ python3 gui.py`

# Dependencies
- numpy           (`$ pip install numpy`)
- prettytable     (`$ pip install prettytable`)
- matplotlib      (`$ pip install matplotlib`)
- PySimpleGUI     (`$ pip install pysimplegui`)

# ToDo
The results with an angled high magnetic field is included is currently a little bit buggy. This should be fixed in the next commit.
