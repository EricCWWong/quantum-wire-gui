import PySimpleGUI as sg
import os
from qwsim_gui.exp_setup import ExpSetup
from qwsim_gui.material import Material

# Column settings in windows 1
first_column = [
    [
        sg.Text("Please enter graphs Settings:")
    ],
    [
        sg.Text("Graph name          "),
        sg.In(size=(15,1), enable_events=False, key="-graph_name-", default_text="conductance")
    ],
    [
        sg.Text("Offset                   "),
        sg.In(size=(4,1), enable_events=False, key="-offset-", default_text="2")
    ],
    [
        sg.Text("Chanels                "),
        sg.In(size=(4,1), enable_events=False, key="-channels-", default_text="5")
    ],
    [
        sg.Text("Please define material properties:")
    ],
    [
        sg.Text("Name                            "),
        sg.In(size=(8,1), enable_events=False, key="-mat_name-", default_text="GaAs"),
    ],
    [
        sg.Text("Lande g-factor                "),
        sg.In(size=(8,1), enable_events=False, key="-mat_g-", default_text="0.04")
    ],
    [
        sg.Text("electron eff mass (m_e)  "),
        sg.In(size=(8,1), enable_events=False, key="-mat_eff-", default_text="0.067")
    ],
    [
        sg.Text("Please choose the range of the parameters:")
    ],
    [
        sg.Text("hbar wx (meV)          "),
        sg.In(size=(4,1), enable_events=False, key="-hbarwx_low-", default_text="1"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-hbarwx_high-", default_text="1")
    ],
    [
        sg.Text("hbar wy (meV)          "),
        sg.In(size=(4,1), enable_events=False, key="-hbarwy_low-", default_text="2"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-hbarwy_high-", default_text="2")
    ],
    [
        sg.Text("eVsd (meV)              "),
        sg.In(size=(4,1), enable_events=False, key="-eVsd_low-", default_text="0"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-eVsd_high-", default_text="0")
    ],
    [
        sg.Text("B (T)                        "),
        sg.In(size=(4,1), enable_events=False, key="-B_low-", default_text="0"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-B_high-", default_text="0")
    ],
    [
        sg.Text("angle (deg)               "),
        sg.In(size=(4,1), enable_events=False, key="-angle_low-", default_text="0"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-angle_high-", default_text="0")
    ],
    [
        sg.Text("energy offset (meV)  "),
        sg.In(size=(4,1), enable_events=False, key="-offset_low-", default_text="0"),
        sg.Text(" - "),
        sg.In(size=(4,1), enable_events=False, key="-offset_high-", default_text="0")
    ],
    [
        sg.Text("Number of curves     "),
        sg.In(size=(4,1), enable_events=False, key="-no_curves-", default_text="5")
    ],
    [
        sg.Button(button_text="Generate Graph with Above Setup", enable_events=True,
                  key="-noneCSVconfirm-")
    ],
    [
        sg.Text("Browse customised setup:")
    ],
    [
        sg.Input(size=(25, 1), key="-files-"),
        sg.FilesBrowse()
    ],
    [
        sg.Button(button_text="Generate Graph with .csv File", enable_events=True,
                  key="-CSVconfirm-")
    ],
    [
        sg.Cancel()
    ]
]

layout = [
    [
        sg.Column(first_column)
    ]
]

window = sg.Window(title="Quantum Wire Simulator", layout=layout)

#Taking inputs from windows 1
event, values = window.read()

csv_read = False

while True:
    if event in (sg.WINDOW_CLOSED, "Cancel") or event =="Exit":
        exit()
    elif event == "-noneCSVconfirm-":
        hbar_w_x_rng = (float(values["-hbarwx_low-"]), float(values["-hbarwx_high-"]))
        hbar_w_y_rng = (float(values["-hbarwy_low-"]), float(values["-hbarwy_high-"]))
        B_field_rng = (float(values["-B_low-"]), float(values["-B_high-"]))
        angle_rng = (float(values["-angle_low-"]), float(values["-angle_high-"]))
        eVsd_rng = (float(values["-eVsd_low-"]), float(values["-eVsd_high-"]))
        energy_off_rng = (float(values["-offset_low-"]), float(values["-offset_high-"]))
        break
    elif event == "-CSVconfirm-":
        if not values["-files-"]:
            sg.popup("Cancel", "No filename supplied")
            raise SystemExit("Cancelling: no files supplied")
        else:
            filename = values["-files-"]
            if filename[-4:] != ".csv":
                sg.popup("Cancel", "File format has to be .csv")
                raise SystemExit("Cancelling: Incorrect file type supplied")
            else:
                csv_read = True
                break

window.close()

no_of_exp = int(values["-no_curves-"])

if csv_read:
    experiments = ExpSetup()
    experiments.set_expsetup(filename)
else:
    experiments = ExpSetup(hbar_w_x_rng, hbar_w_y_rng, eVsd_rng,
                               B_field_rng, angle_rng, energy_off_rng, no_of_exp)

material = Material(name=values["-mat_name-"],
                    g=float(values["-mat_g-"]), me_eff=float(values["-mat_eff-"]))

experiments.plotter(material, int(values["-channels-"]), float(values["-offset-"]),
                    graph_name= values["-graph_name-"])
