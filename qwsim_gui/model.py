import numpy as np
from .constants import h_bar
from .model_helper import heaviside
from .material import Material

class Model:
    def __init__(self, material, hw_x, hw_y, V_sd=0, magnetic_field=0, angle=0, energy_offset=0):
        
        # Basic class attributes:
        self.material = material
        assert type(self.material) == Material, "material must be Material type!"
        
        self.hw_x = hw_x
        self.hw_y = hw_y
        self.eVsd = V_sd
        self.magnetic_field = magnetic_field
        self.angle = np.deg2rad(angle)
        self.energy_offset = energy_offset

        # Cyclotron/Magnetic terms:
        self.hw_c = h_bar * self.magnetic_field * np.sin(self.angle) / self.material.m_e_eff * 10**3
        if np.abs(self.hw_c) < 0.0000001:
            self.hw_c = 0
        self.angular_freq = self.hw_c ** 2 + self.hw_y ** 2 - self.hw_x ** 2
        self.zeeman = self.material.get_zeeman(self.magnetic_field)

        # Advanced energy terms:
        self.E1 = 1/(2 * np.pi * np.sqrt(2)) * ((self.angular_freq**2 + 4 * self.hw_x**2 * self.hw_y**2)**(1/2) - self.angular_freq)**(1/2)
        self.E2 = 1/np.sqrt(2) * ((self.angular_freq**2 + 4 * self.hw_x**2 * self.hw_y**2)**(1/2) + self.angular_freq)**(1/2)

    # Transmission terms:
    def forward_transmission(self, n, x):
        if n == 0:
            partial_energy_term = self.hw_x * x + 1/2 * self.eVsd - (n + 1/2) * self.E2 - self.energy_offset
        else:
            partial_energy_term = self.hw_x * x + 1/2 * self.eVsd - (n + 1/2) * self.E2
        t_forward = 1/2 * (heaviside(-1/self.E1 * (partial_energy_term + self.zeeman)) +
                            heaviside(-1/self.E1 * (partial_energy_term - self.zeeman)))
        return t_forward

    def backward_transmission(self, n, x):
        if n == 0:
            partial_energy_term = self.hw_x * x - 1/2 * self.eVsd - (n + 1/2) * self.E2 - self.energy_offset
        else:
            partial_energy_term = self.hw_x * x - 1/2 * self.eVsd - (n + 1/2) * self.E2
        t_backward = 1/2 * (heaviside(-1/self.E1 * (partial_energy_term + self.zeeman)) +
                            heaviside(-1/self.E1 * (partial_energy_term - self.zeeman)))
        return t_backward

    def total_transmission(self, channels, x):
        output = 0
        for i in range(0, channels):
            output = output + self.forward_transmission(i, x) + self.backward_transmission(i, x)
        return output / 2
