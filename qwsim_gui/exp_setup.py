from .material import Material
from .model import Model
import numpy as np
from matplotlib import pyplot as plt
from prettytable import PrettyTable

class ExpSetup:
    def __init__(self, hw_x_rng=(1,1), hw_y_rng=(1,1), Vsd_rng=(0,0),
                 B_rng=(0,0), angle_rng=(0,0), offset_rng=(0,0), no_exp=5):
        self.hw_x_rng = hw_x_rng
        self.hw_y_rng = hw_y_rng
        self.Vsd_rng = Vsd_rng
        self.B_rng = B_rng
        self.angle_rng = angle_rng
        self.offset_rng = offset_rng
        self.no_exp = no_exp

        self.exp_setup = []

        for i in range(no_exp):
            hbar_w_x = self.hw_x_rng[0] + i * (self.hw_x_rng[1] - self.hw_x_rng[0]) / self.no_exp
            hbar_w_y = self.hw_y_rng[0] + i * (self.hw_y_rng[1] - self.hw_y_rng[0]) / self.no_exp
            B_field = self.B_rng[0] + i * (self.B_rng[1] - self.B_rng[0]) / self.no_exp
            angle = self.angle_rng[0] + i * (self.angle_rng[1] - self.angle_rng[0]) / self.no_exp
            eVsd = self.Vsd_rng[0] + i * (self.Vsd_rng[1] - self.Vsd_rng[0]) / self.no_exp
            energy_offset = self.offset_rng[0] + i * (self.offset_rng[1] -
                                                     self.offset_rng[0]) / self.no_exp

            row = [hbar_w_x, hbar_w_y/hbar_w_x, eVsd, B_field, angle, energy_offset]
            self.exp_setup.append(row)

    def get_expsetup(self):
        return self.exp_setup

    def set_expsetup(self, file_name):
        """
        This will take a .csv file and retrieve the experimental setup.

        Parameters
        ----------
        filename: str
            The filename that contains the experiment setup in .csv format.

        Returns
        -------
        exp_setup: array
            This is the array for experiment setup.
        """

        exp_setup = np.genfromtxt(
            file_name,
            delimiter=",",
            skip_header=1)
        if exp_setup.ndim == 1:
            exp_setup = [exp_setup]
            exp_setup = np.array(exp_setup)

        self.exp_setup = exp_setup
        return exp_setup

    def plotter(self, material, channels, offset, graph_name='conductance', plot_diff=True):
        """
        This will take in different experiment setup and plot the predicted graph.

        Parameters
        ----------
        material: object (Material)
            This object stores the data such as Lande g factor, effective electron
            mass for specific material.
        exp_setup: 2d-array
            The experiment setup, see the csv file format for details.
        channels: int
            The number of electrons subband taken into account.
        graph_name: str
            The name of the plot. Default is 'conductance'.
        fmt: str
            The format of the saved file. Default is pdf.
        savefig: str
            The name of the fig if user wants it to be saved. If savefig is NONE, then
            the figure will not be saved.

        Returns
        -------
        The required plot.
        """

        exp_setup = self.get_expsetup()

        # Initialise graph:
        fig, axs = plt.subplots(1, 1)
        fig.suptitle(graph_name)
        axs.set_ylabel(r'$G \times \frac{h}{2e^{2}} $')
        axs.set_xlabel(r'$ \frac{E_{f} - U_{0}}{\hbar w_{x}}$')
        axs.set_ylim([0, channels])
        axs.set_yticks(np.arange(0, channels + 0.25, 0.25))
        plt.grid()

        # Initialise table:
        table = PrettyTable()
        table.field_names = ['hbar w_x (meV)', 'w_y/ w_x', 'B (T)', 'angle (rad)', 'E1 (meV)', 'E2 (meV)', 'eVsd (meV)', 'hbar w_c (meV)', 'Zeeman (meV)', 'Energy Offset (meV)']


        # Setting up conductance plot:
        plots = len(exp_setup)
        x = np.arange(-2, offset * plots + channels * 6, 0.1)

        # Setting up differential plot:
        x_diff = np.arange(-2, 30, 0.1)
        dydx = []

        for i in range(plots):
            # initialise experiment setup:
            experiment = exp_setup[i]
            hw_x = experiment[0]
            hw_y = experiment[1] * hw_x
            V_sd = experiment[2]
            B = experiment[3]
            angle = experiment[4]
            energy_offset = experiment[5]

            # constructing model:
            model = Model(material, hw_x, hw_y, V_sd, B, angle, energy_offset)
            # adding row data in table:
            table.add_row([model.hw_x, model.hw_y/model.hw_x, model.magnetic_field, model.angle, model.E1, model.E2, model.eVsd, model.hw_c, model.zeeman, model.energy_offset])


            # calculate y values:
            y = model.total_transmission(channels, x - i * offset)
            y_diff = model.total_transmission(channels, x_diff)
            axs.plot(x,y)
            if i == (plots-1):
                min = np.where(y >= channels - 0.1)
                x_min_index = min[0][0]
                x_min = x[x_min_index] + 1

            dydx.append(np.gradient(y_diff))

        print(table)
        print('Note: Parallel field has angle 0 rad.')

        axs.set_xlim([-1, x_min])

        if plots > 1 and plot_diff is True:
            # Initialise diff graph:
            fig1, axs1 = plt.subplots(1, 1)
            fig1.suptitle(graph_name + " (Differential Plot)")
            axs1.set_xlabel(r'$ 10 \times E_{f}$')
            axs1.set_ylabel('Experiments')
            axs1.set_xlim([0, (x_min - (plots - 3)* offset) *10])

            dydx = np.array(dydx)
            contour = axs1.contourf(dydx)
            fig1.colorbar(contour)

        plt.show()


if __name__ == "__main__":
    # Define the settings:
    hbar_w_x_rng = (1,1)
    hbar_w_y_rng = (2,2)
    B_field_rng = (0,10)
    angle_rng = (10,10)
    eVsd_rng = (0,0)
    energy_off_rng = (0,0)

    no_of_exp = 20

    experiments = ExpSetup(hbar_w_x_rng, hbar_w_y_rng, eVsd_rng,
                           B_field_rng, angle_rng, energy_off_rng, no_of_exp)

    material = Material(name="GaAs")

    experiments.plotter(material, 5, 2)
