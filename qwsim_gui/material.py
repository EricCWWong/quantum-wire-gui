from .constants import mu_B

class Material:
    def __init__(self, name="", g=0.04, me_eff=0.067):
        self.name = name
        self.m_e = 9.31 * 10 ** (-31)
        self.m_e_eff = me_eff * self.m_e
        self.g = g

    def get_zeeman(self, magnetic_field, S=1/2):
        zeeman = self.g * mu_B * S * magnetic_field
        return zeeman
