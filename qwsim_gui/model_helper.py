import numpy as np

def heaviside(arg):
    return 1/(1+np.exp(arg))